# HyperGeoMetric2F1
HyperGeoMetric2F1 is a primitive math tool for mathematicians that research Hypergeometric2F1 functions, their relations or continued fractions for those function relations in general form.

Program allow to build relations for Hypergeometric2F1 functions and allow to build continued fractions for Hypergeometric2F1 relations.

HyperGeoMetric2F1 is "Console Application".
Form2F1 is "Windows Forms Application".
Code is written in C# for ".NET Framework" v2.0 or higher.
